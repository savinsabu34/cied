import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftDashboardAreaComponent } from './left-dashboard-area.component';

describe('LeftDashboardAreaComponent', () => {
  let component: LeftDashboardAreaComponent;
  let fixture: ComponentFixture<LeftDashboardAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftDashboardAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftDashboardAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
