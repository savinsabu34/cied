import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LiveDashboardRoutingModule } from './live-dashboard-routing.module';
import { LiveDashboardComponent } from './components/live-dashboard/live-dashboard.component';
import { AppMaterialModule } from 'src/app/common/material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LeftDashboardAreaComponent } from './components/left-dashboard-area/left-dashboard-area.component';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../../app.module';

@NgModule({
  declarations: [LiveDashboardComponent, LeftDashboardAreaComponent],
  imports: [
    CommonModule,
    AppMaterialModule,
    FormsModule,
    HttpClientModule,
    FlexLayoutModule,
    LiveDashboardRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ]
})
export class LiveDashboardModule { }
