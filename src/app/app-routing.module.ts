import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // { path: 'agricalc', loadChildren: './modules/live-dashboard/live-dashboard.module#LiveDashboardModule'}
  { path: '', pathMatch: 'full', redirectTo: 'agricalc'},
  { path: 'agricalc', loadChildren: () => import("./modules/live-dashboard/live-dashboard.module").then(m => m.LiveDashboardModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
